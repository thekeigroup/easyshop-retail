<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $table = 'customers';

    protected $fillable = ['first_name', 'last_name', 'gender', 'email_address', 'dob'];

    public function reviews()
    {
        return $this->hasMany('App\Review', 'customer_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Order', 'customer_id');
    }

    public function wishlists()
    {
        return $this->hasMany('App\Wishlist', 'customer_id');
    }

    public function addresses()
    {
        return $this->hasMany('App\Address', 'user_id');
    }

    public function getShippingAddress()
    {
        $address = $this->addresses()->where('type', '=', 'SHIPPING')->first();
        return $address;
    }

    public function getBillingAddress()
    {
        $address = $this->addresses()->where('type', '=', 'BILLING')->first();
        return $address;
    }

    public function isInWishlist($productId)
    {
        $wishList = WishList::where('customer_ids', '=', $this->attributes['id'])
            ->where('product_id', '=', $productId)->get();
        if (count($wishList) <= 0) {
            return false;
        }

        return true;
    }
}
