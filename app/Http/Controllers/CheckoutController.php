<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CheckoutController extends Controller
{
    //
    public function index()
    {
        $cartItems = Session::get('cart');

        return view('checkout.checkout')
            ->with('cartItems', $cartItems);
    }
}
