<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Address;
use App\Http\Requests\AddressRequest;
use App\Helpers\CommonServiceInterface as CommonService;


class AddressController extends Controller
{

    public function __construct(CommonService $commonService)
    {
        return $this->commonService = $commonService;
    }

    public function index()
    {
        $user = Auth::user();
        $customer = $this->commonService->findCustomerByUserEmail($user->email);
        $shippingAddress = $customer->getShippingAddress();
        $billingAddress = $customer->getBillingAddress();

        return view('customer.address')
            ->with('customer', $customer)
            ->with('shippingAddress', $shippingAddress)
            ->with('billingAddress', $billingAddress);

        return view('customer.address');
    }

    public function create()
    {
        $user = Auth::user();
        $address = new Address();
        $customer = $this->commonService->findCustomerByUserEmail($user->email);
        return view('forms.customer.address.create')
            ->with('user', $customer)
            ->with('address', $address);
    }

    public function store(AddressRequest $request)
    {
        $address = new Address();
        $user = Auth::user();
        $customer = $this->commonService->findCustomerByUserEmail($user->email);
        $address->first_name = $request->get('first_name');
        $address->last_name = $request->get('last_name');
        $address->address1 = $request->get('address1');
        $address->address2 = $request->get('address2');
        $address->phone = $request->get('phone');
        $address->type = $request->get('type');

        $address->customer()->associate($customer);
        if ($address->save()) {
            return redirect()->route('customer.address.index')
                ->with('success', 'Address saved successfully');
        } else {
            return redirect()->back()
                ->with('error', 'Failed to save address');
        }
    }

    public function edit($id)
    {
        $user = Auth::user();
        $address = Address::findOrfail($id);
        $customer = $this->commonService->findCustomerByUserEmail($user->email);
        return view('forms.customer.address.edit')
            ->with('address', $address)
            ->with('user', $customer);
    }

    public function update(AddressRequest $request, $id)
    {
        $address = Address::findOrfail($id);
        if ($address->update($request->all())) {
            return redirect()->route('customer.address.index')
                ->with('success', 'Address updated successfully');

        } else {
            return redirect()->back()
                - with('error', 'Failed to update address');
        }
    }

    public function destroy($id)
    {
        if (Address::destroy($id)) {
            return redirect()->route('customer.address.index')
                ->with('success', 'Address removed successfully');
        } else {
            return redirect()->route('customer.address.index')
                ->with('error', 'Failed to remove address');
        }
    }

}
