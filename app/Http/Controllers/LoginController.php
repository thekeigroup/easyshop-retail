<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\LoginRequest;
use TCG\Voyager\Models\Role;
use App\Customer;
use App\Supplier;

class LoginController extends Controller
{
    //

    public function authenticate(LoginRequest $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');

        //dd(bcrypt($password));
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            //authentication password

            $user = Auth::user();
            //get role id from authenticated user
            //dd($user);
            $role_id = $user->role_id;

            //dd($role_id);

            $role_name = Role::findorfail($role_id)->name;

            if ($role_name === 'user') {
                //fetch customer
                $customer = Customer::where('email_address', '=', $user->email)->first();
                return redirect()->route('customer.dashboard')
                    ->with('customer', $customer);
            }
            if ($role_name === 'supplier') {
                $supplier = Supplier::where('email', '=', $user->email)->first();
                return redirect()->route('supplier.my-account');
            }

        } else {
            return redirect()->route('login')->with('error', 'Wrong username or password');
        }
    }
}
