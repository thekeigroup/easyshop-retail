<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Requests\GiftCouponRequest;
use App\Http\Controllers\Controller;
use App\GiftCoupon;
use Illuminate\Support\Facades\Auth;
use App\Helpers\CommonServiceInterface as CommonService;

class GiftCouponController extends Controller
{
    //
    public function __construct(CommonService $commonService)
    {
        $this->commonService = $commonService;
    }

    public function index()
    {
        $user = Auth::user();
        $supplier = $this->commonService->findSupplierByUserEmail($user->email);

        $giftCoupons = $supplier->giftCoupons;

        return view('supplier.gift-coupons')
            ->with('giftCoupons', $giftCoupons);
    }

    public function create()
    {
        $giftCoupon = new GiftCoupon();
        return view('forms.supplier.gift-coupon.create')
            ->with('giftCoupon', $giftCoupon);
    }

    public function store(GiftCouponRequest $request)
    {
        $giftCoupon = new GiftCoupon();
        $user = Auth::user();
        $userEmail = $user->email;
        $supplier = $this->commonService->findSupplierByUserEmail($userEmail);

        $giftCoupon->name = $request->get('name');
        $giftCoupon->code = $request->get('code');
        $giftCoupon->discount = $request->get('discount');
        $giftCoupon->start_date = $request->get('start_date');
        $giftCoupon->end_date = $request->get('end_date');
        $giftCoupon->status = $request->get('status');

        $giftCoupon->supplier()->associate($supplier);
        if($giftCoupon->save()){
            return redirect()->route('supplier.gift-coupon.index')
                ->with('success', 'Gift coupon created successfully');
        }else{
            return redirect()->back()
                ->with('error', 'Failed to create gift coupon');
        }
    }

    public function edit($id)
    {
        $giftCoupon = GiftCoupon::find($id);
        return view('forms.supplier.gift-coupon.edit')
            ->with('giftCoupon', $giftCoupon);

    }

    public function update(GiftCouponRequest $request,$id)
    {
        $giftCoupon = GiftCoupon::findorfail($id);
        $giftCoupon->update($request->all());
        return redirect()->route('supplier.gift-coupon.index')
            ->with('success', 'Gift coupon updated successfully');
    }

    public function destroy($id)
    {
        GiftCoupon::destroy($id);
        return redirect()->route('supplier.gift-coupon.index')
            ->with('success', 'Gift coupon removed successfully');
    }
}
