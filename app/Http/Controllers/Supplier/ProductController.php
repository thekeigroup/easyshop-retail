<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Requests\ProductRequest;
use App\Product;
use App\ProductImage;
use Illuminate\Http\Request;
use App\Helpers\CommonServiceInterface as CommonService;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    //
    public function __construct(CommonService $commonService)
    {
        $this->commonService = $commonService;
    }

    public function index()
    {
        $user = Auth::user();
        $supplier = $this->commonService->findSupplierByUserEmail($user->email);
        $products = $this->commonService->getSupplierProducts($supplier->id);
        return view('supplier.products')->with(['products' => $products]);
    }

    public function create()
    {
        return view('forms.supplier.product.create');
    }

    public function store(ProductRequest $request)
    {
        $user = Auth::user();
        $supplier = $this->commonService->findSupplierByUserEmail($user->email);
        $product = new Product();
        $product->product_name = $request->get('product_name');
        $product->description = $request->get('description');
        $product->product_name = $request->get('product_name');
        $product->slug = $request->get('slug');
        $product->qty = $request->get('qty');
        $product->page_description = $request->get('page_description');
        $product->track_stock = (bool)$request->get('track_stock');
        $product->in_stock = (bool)$request->get('in_stock');
        $product->page_title = $request->get('page_title');
        $product->sku = $request->get('sku');
        $product->published = (bool)$request->get('published');
        $product->supplier()->associate($supplier);
        if ($product->save()) {
            //save product price
            $product->prices()->create(['price' => $request->get('price')]);
            return $this->uploadImage($product);
        }else{
            return redirect()->back()->withInput()->with('error', "Error adding product");
        }
    }

    public function edit($id)
    {
        $product = Product::findorfail($id);
        return view('forms.supplier.product.edit')
            ->with('model', $product);
    }

    public function update(Request $request, $id)
    {
        $product = Product::findorfail($id);
        if ($product->saveProduct($request)) {
            return redirect()->route('supplier.products');
        }
    }

    public function destroy($id)
    {
        Product::destroy($id);
        return redirect()->route('supplier.products');
    }

    public function uploadImage($product)
    {
        return view('forms.supplier.product.upload-image')
            ->with('product', $product)
            ->with('success', 'Product added successfully. Add product images.');
    }

    public function storeImage(Request $request)
    {
        $product = Product::findOrfail($request->get('product_id'));

        $productImage = new ProductImage();
        if ($request->hasFile('product_image')) {
            $file = $request->file('product_image');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('public')->put($file->getFilename() . '.' . $extension, File::get($file));
            $productImage->path = $file->getFilename() . '.' . $extension;
        }
        $productImage->is_main_image = (bool)$request->get('is_main_image');
        $productImage->product()->associate($product);

        if ($productImage->save()) {
            return $this->uploadImage($product)
                ->with('success', 'Product image added successfully.');
        }
    }

}
