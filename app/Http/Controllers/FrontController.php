<?php

namespace App\Http\Controllers;

use App\Http\Requests\BecomeSupplierRequest;
use App\Supplier;
use App\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Helpers\CommonServiceInterface as CommonService;
use TCG\Voyager\Models\User;


class FrontController extends Controller
{

    public function home()
    {
        return view('index');
    }

    public function signup()
    {
        return view('signup');
    }

    public function register(Request $request)
    {
        $role_customer = 'user';
        $customer = new Customer();

        $customer->first_name = $request->get('first_name');
        $customer->last_name = $request->get('last_name');
        $customer->gender = $request->get('gender');
        $customer->email_address = $request->get('email');
        $customer->dob = $request->get('dob');

        if ($customer->save()) {
            //create new user
            $user = new User();
            $user->email = $request->get('email');
            $user->name = $request->get('last_name') . ' ' . $request->get('first_name');
            $user->password = bcrypt($request->get('pwd'));
            $user->setRole($role_customer);
            $user->save();

            //we will redirect to customer to login
            return redirect()->route('login')
                ->with('success', 'Account successfully created. Login to manage account');

        }else{
            return redirect()->withInput()->route('signup')
                ->with('error', 'Failed to create account');
        }
    }

    public function login()
    {
        return view('login');
    }



    public function becomeSupplier()
    {
        return view('become-a-supplier');
    }

    public function registerSupplier(BecomeSupplierRequest $request)
    {
        $supplier = new Supplier();

        $supplier->first_name = $request->get('first_name');
        $supplier->last_name = $request->get('last_name');
        $supplier->preferred_name = $request->get('preferred_name');
        $supplier->mobile_phone_no = $request->get('mobile_phone_no');
        $supplier->email = $request->get('email');
        $supplier->supplier_type = $request->get('supplier_type');
        if ($supplier->save()){
            //create user for supplier
            $user = new User();
            $user->email = $request->get('email');
            $user->name = $request->get('last_name') . ' ' . $request->get('first_name');
            $user->password = bcrypt($request->get('password'));
            $role_supplier = 'supplier';
            $user->setRole($role_supplier);
            if ($user->save()) {
                //redirect to login page
                return redirect()->route('login')
                    ->with('success', 'Your account created successfully. Log in to start managing account');
            } else {
                return redirect()->back()->withInput()
                    ->with('error', 'Failed to create supplier account')
                    ->withErrors();
            }
        }

    }

    public function logout()
    {
        Auth::logout();
        //Redirect to login page
        return redirect()->route('login');
    }

    public function placeOrder()
    {

    }

}
