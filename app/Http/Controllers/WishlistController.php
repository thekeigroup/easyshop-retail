<?php

namespace App\Http\Controllers;

use App\Helpers\CommonService;
use App\Wishlist;
use Illuminate\Support\Facades\Auth;
use App\Helpers\CommonServiceInterface as CommonsService;

class WishlistController extends Controller
{
    //
    public function __construct(CommonService $commonService)
    {
        $this->commonService = $commonService;
    }

    public function index()
    {
        $user = Auth::user();
        $customer = $this->commonService->findCustomerByUserEmail($user->email);
        $wishlists = $customer->wishlists();
        return view('customer.wishlist')
            ->with('wishlists', $wishlists);
    }

    public function add($slug)
    {
        $user = Auth::user();
        $customer = $this->commonService->findCustomerByUserEmail($user->email);
        $product = Product::getProductBySlug($slug);
        Wishlist::create([
            'customer_id' => $customer->id,
            'product_id' => $product->id
        ]);

        return redirect()->back()
            ->with('notification', 'Product added into your Wishlist successfully');

    }

    public function destroy($slug)
    {
        $user = Auth::user();
        $customer = $this->commonService->findCustomerByUserEmail($user->email);
        $product = Product::getProductBySlug($slug);

        Wishlist::where([
            'customer_id' => $customer->id,
            'product_id' => $product->id,
        ])->delete();
        return redirect()->back()->with('notification', 'Product removed from your wishlist successfully!!');
    }

}
