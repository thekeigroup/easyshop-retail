<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\CommonServiceInterface as CommonService;
use Illuminate\Support\Facades\Auth;
use App\Order;
use App\OrderStatus;

class OrderController extends Controller
{
    //
    public function __construct(CommonService $commonService)
    {
        return $this->commonService = $commonService;
    }

    public function index()
    {
        //fetch customer
        $user = Auth::user();
        $customer = $this->commonService->findCustomerByUserEmail($user->email);

        $orders = $customer->orders;

        return view('customer.order')
            ->with('orders', $orders);
    }

    public function view($orderNumber)
    {

    }
    public function destroy($id)
    {

    }
}
