<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ReviewRequest;
use App\Helpers\CommonServiceInterface as CommonService;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    //

    public function __construct(CommonService $commonService)
    {
        return $this->commonService = $commonService;
    }

    public function index()
    {
        $user = Auth::user();
        $customer = $this->commonService->findCustomerByUserEmail($user->email);
        $reviews = $customer->reviews();
        return view('customer.reviewsratings')
            ->with('reviews', $reviews);
    }

    public function store(ReviewRequest $request)
    {

    }

    public function destroy($id)
    {

    }
}
