<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\CommonServiceInterface as CommonService;
use App\Product;
use App\ProductImage;

class SearchController extends Controller
{
    //
    public function __construct(CommonService $commonService)
    {
        $this->commonService = $commonService;
    }


    public function search(Request $request)
    {
        $q = $request->get('q');
        $products = $this->commonService->searchProducts($q);
        return view('catalog')->with('products', $products);
    }

    public function itemDetails($slug)
    {
        $product = Product::where('slug', '=', $slug)->get()->first();
        return view('includes.catalog.single-product')
            ->with('product', $product);
    }
}
