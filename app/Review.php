<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    //
    protected $table = 'reviews';
    
    protected $fillable = [
        'product_id', 'customer_id', 'star', 'comment', 'is_enabled'
    ];
    
    public function customer() {
        return $this->belongsTo('App\Customer', 'customer_id');
    }
    
    public function product() {
        return $this->belongsTo('App\Product', 'product_id');
    }
    
    public function getProductNameAttribute() {
        return $this->product->product_name;
    }
    
    public function getUserFullNameAttribute() {
        return $this->user->full_name;
    }
}
