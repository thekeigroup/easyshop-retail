<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    //
    protected $table = 'wishlists';

    protected $fillable = ['customer_id', 'product_id'];

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id');
    }
}
