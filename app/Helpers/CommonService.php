<?php

namespace App\Helpers;

use App\Product;
use App\Order;
use App\Supplier;
use App\Customer;

class CommonService implements CommonServiceInterface
{

    public function loadProducts($rows)
    {
        $products = Product::paginate($rows);
        return $products;
    }

    public function searchCatalog($searchType, $searchString)
    {
        if ($searchType == 'Product') {

        }
    }

    public function searchProducts($search)
    {
        $products = Product::where('product_name', 'like', '%' . $search . '%')->get();
        return $products;
    }

    public function getSupplierProducts($supplierId)
    {
        $products = Product::with('images')->where('supplier_id', '=', $supplierId)->get();
        return $products;
    }

    public function getCustomerOrders($customerId)
    {
        $orders = Order::where('user_id', '=', $customerId)->get();
        return $orders;
    }

    public function getSupplierOrders($supplierId)
    {

    }

    public function findSupplierByUserEmail($userEmail)
    {
        $supplier = Supplier::where('email', '=', $userEmail)->first();
        return $supplier;
    }

    public function getSupplierGiftCoupons($supplierId)
    {

    }

    public function findCustomerByUserEmail($userEmail)
    {
        $customer = Customer::where('email_address', '=', $userEmail)->first();
        return $customer;
    }
}
