<?php

namespace App\Helpers;

interface CommonServiceInterface {

	public function loadProducts($rows);
	public function searchCatalog($searchType, $searchString);
	public function searchProducts($searchString);
	public function getSupplierProducts($supplierId);
	public function getCustomerOrders($customerId);
    public function getSupplierOrders($supplierId);
    public function getSupplierGiftCoupons($supplierId);
    public function findSupplierByUserEmail($userEmail);
    public function findCustomerByUserEmail($userEmail);

}