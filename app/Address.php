<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //
    protected $table = 'addresses';
    
    protected $fillable = [ 'type', 'first_name','last_name', 'address1', 'address2', 'postcode', 'city', 'phone', 'country_id'];
    
    public function country() {
        return $this->belongsTo(Country::class);
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'user_id');
    }

}
