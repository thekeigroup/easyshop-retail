<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
    //
    protected $table = 'orders';
    protected $fillable = ['shipping_address_id','billing_address_id','user_id','shipping_option','payment_option', 'order_status_id'];

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function orderStatus()
    {
        return $this->belongsTo('App\OrderStatus');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier', 'supplier_id');
    }

    public function orderProducts()
    {
        return $this->hasMany('App\OrderProduct');
    }

}
