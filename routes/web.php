<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [
    'as' => 'home',
    'uses' => 'FrontController@home'
]);
Route::get('/catalog', [
    'as' => 'catalog.search',
    'uses' => 'SearchController@search'
]);
Route::get('/catalog/product-details/{slug}', [
   'as' => 'catalog.item',
   'uses' => 'SearchController@itemDetails'
]);

//------------------------public routes------------------
Route::get('/account/become-supplier', [
    'as' => 'become-supplier',
    'uses' => 'FrontController@becomeSupplier'
]);
Route::post('/account/register', [
    'as' => 'register-supplier',
    'uses' => 'FrontController@registerSupplier'
]);

Route::get('/account/login', [
    'as' => 'login',
    'uses' => 'FrontController@login'
]);

Route::get('logout', [
    'as' => 'logout',
    'uses' => 'FrontController@logout'
]);

Route::post('/account/authenticate', [
    'as' => 'authenticate',
    'uses' => 'LoginController@authenticate'
]);

Route::get('/account/signup', [
    'as' => 'signup',
    'uses' => 'FrontController@signup'
]);

Route::post('/customer/create', [
    'as' => 'customer.register',
    'uses' => 'FrontController@register'
]);

Route::post('/cart/add-to-cart',[
   'as' => 'cart.add-to-cart',
    'uses'=> 'CartController@addToCart'
]);
Route::get('/cart/view', [
    'as' => 'cart.view',
    'uses' => 'CartController@view'
]);
Route::put('/cart/update',[
    'as' => 'cart.update',
    'uses' => 'CartController@update'
]);
Route::get('/cart/destroy/{id}', [
    'as' => 'cart.destroy',
    'uses' => 'CartController@destroy'
]);

Route::get('/checkout',[
   'as' => 'checkout',
   'uses' => 'CheckoutController@index'
]);
//----------------end of public routes------------

//-----------route to get uploaded images--------------
Route::get('fileentry/{filename}', [
    'as' => 'product.image',
    'uses' => 'SupplierController@getProductImage'
]);


//-----------------------end----------------------------
//--------------------supplier routes-------------------
Route::group(['prefix' => 'supplier'], function () {
    Route::get('/account/index', [
        'as' => 'supplier.my-account',
        'uses' => 'Supplier\SupplierController@myAccount'
    ]);
    Route::get('/product/index', [
        'as' => 'supplier.product.index',
        'uses' => 'Supplier\ProductController@index'
    ]);
    Route::get('/product/create', [
        'as' => 'supplier.product.create',
        'uses' => 'Supplier\ProductController@create'
    ]);
    Route::post('/product/index', [
        'as' => 'supplier.product.store',
        'uses' => 'Supplier\ProductController@store'
    ]);
    Route::get('/product/upload-image', [
        'as' => 'supplier.product.upload-image',
        'uses' => 'Supplier\ProductController@uploadImage'
    ]);
    Route::post('/product/store-image', [
        'as' => 'supplier.product.store-image',
        'uses' => 'Supplier\ProductController@storeImage'
    ]);
    Route::get('/order/index', [
        'as' => 'supplier.order.index',
        'uses' => 'Supplier\OrderController@index'
    ]);
    Route::get('/order/{id}/view', [
        'as' => 'supplier.order.view',
        'uses' => 'Supplier\OrderController@view'
    ]);
    Route::get('/order/{id}/change-status', [
        'as' => 'supplier.order.change-status',
        'uses' => 'Supplier\OrderController@changeStatus'
    ]);
    Route::put('/order/{id}/update-status', [
        'as' => 'supplier.order.update-status',
        'uses' => 'Supplier\OrderController@updateStatus'
    ]);
    Route::get('/gift-coupon/index', [
        'as' => 'supplier.gift-coupon.index',
        'uses' => 'Supplier\GiftCouponController@index'
    ]);
    Route::get('/gift-coupon/create', [
        'as' => 'supplier.gift-coupon.create',
        'uses' => 'Supplier\GiftCouponController@create'
    ]);
    Route::post('/gift-coupon/', [
        'as' => 'supplier.gift-coupon.store',
        'uses' => 'Supplier\GiftCouponController@store'
    ]);
    Route::get('/gift-coupon/{id}/edit', [
        'as' => 'supplier.gift-coupon.edit',
        'uses' => 'Supplier\GiftCouponController@edit'
    ]);
    Route::put('/gift-coupon/{id}/update', [
        'as' => 'supplier.gift-coupon.update',
        'uses' => 'Supplier\GiftCouponController@update'
    ]);
    Route::get('/gift-coupon/{id}', [
        'as' => 'supplier.gift-coupon.delete',
        'uses' => 'Supplier\GiftCouponController@destroy'
    ]);

});
//-----------------------end-----------------------------

Route::group(['prefix' => 'customer'], function () {
    Route::get('/account', [
        'as' => 'customer.dashboard',
        'uses' => 'CustomerController@home'
    ]);
    //--------------address routes------------------
    Route::get('/address/index', [
        'as' => 'customer.address.index',
        'uses' => 'AddressController@index'
    ]);
    Route::get('/address/create', [
        'as' => 'customer.address.create',
        'uses' => 'AddressController@create'
    ]);
    Route::post('/address/store', [
        'as' => 'customer.address.store',
        'uses' => 'AddressController@store'
    ]);
    Route::get('/address/edit/{id}', [
        'as' => 'customer.address.edit',
        'uses' => 'AddressController@edit'
    ]);
    Route::put('/address/update/{id}', [
        'as' => 'customer.address.update',
        'uses' => 'AddressController@update'
    ]);
    Route::get('/address/{id}', [
        'as' => 'customer.address.delete',
        'uses' => 'AddressController@destroy'
    ]);
    //-------------end---------------------
    //----------Order routes---------------
    Route::get('/order/index', [
        'as' => 'customer.order.index',
        'uses' => 'OrderController@index'
    ]);
    Route::get('/order/details/{id}', [
       'as' => 'customer.order.details',
       'uses' => 'OrderController@details'
    ]);
    //-------------end-----------------
    //----------wishlist routes---------
    Route::get('/wishlist/index', [
        'as' => 'customer.wishlist.index',
        'uses' => 'WishlistController@index'
    ]);
    Route::get('/wishlist/add/{slug}', [
       'as' => 'wishlist.add',
       'uses' => 'WishlistController@add'
    ]);
    Route::delete('/wishlist/destroy/{slug}', [
        'as' => 'customer.wishlist.remove',
        'uses' => 'WishlistController@destroy'
    ]);

    //------------end--------------------
    //------------reviews route-----------
    Route::get('/reviewsratings/index', [
        'as' => 'customer.reviewsratings.index',
        'uses' => 'ReviewController@index'
    ]);
    Route::post('/reviewsratings/store', [
       'as' => 'customer.reviewsratings.store',
       'uses' => 'ReviewController@store'
    ]);
    //---------------end--------------------


});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
