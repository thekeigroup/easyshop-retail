<ul>
    <li><a class="active" href="{{URL::route('supplier.my-account')}}">Account</a></li>
    <li><a href="{{URL::route('supplier.product.index')}}">Products</a></li>
    <li><a href="{{URL::route('supplier.order.index')}}">Orders</a></li>
    <li><a href="#">Reviews</a></li>
    <li><a href="{{URL::route('supplier.gift-coupon.index')}}">Gift Coupons</a></li>

    <li class="pull-right"><a href="{{route('logout')}}">Logout</a> </li>
</ul>
