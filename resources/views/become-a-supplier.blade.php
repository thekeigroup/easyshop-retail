@extends('layouts._registration')
@section('title')
    Become A Supplier
@endsection
@section('header-text')
    Supplier Registration
@endsection
@section('descriptive-text')
    Please fill form below
@endsection
@section('content')

    @include('forms.supplier.registration.signup')
@endsection()
