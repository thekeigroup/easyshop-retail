{!! Form::open(array('route' => 'customer.register', 'method' => 'POST')) !!}

    <div class="row">
        <div class="col-md-6">
            <div class="input-container">
                <label>First Name</label>
                <input type="text"
                       name="first_name"
                       class="input-element"/>
                <!-- if the input field has an error add class : error -->
                <span class="input-error"></span>
                <!-- if the input field has an error add text in this span -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-container">
                <label>Last Name</label>
                <input type="text"
                       name="last_name"
                       class="input-element"/>
                <span class="input-error"></span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="input-container">
                <label>Gender</label>
                <select class="input-element" name="gender">
                    <option value="">Select Gender</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                </select>
                <span class="input-error"></span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-container">
                <label>Birthday</label>
                <input type="text"
                       name="date_of_birth"
                       class="input-element datepicker-here"
                       data-language="en"
                       data-date-format="yyyy-mm-dd"/>
                <span class="input-error"></span>
            </div> <!-- Date picker ref: http://t1m0n.name/air-datepicker/docs/ -->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="input-container">
                <label>Email</label>
                <input type="email"
                       name="email"
                       class="input-element"/>
                <span class="input-error"></span>
            </div>
        </div>
    </div>

    <hr/>

    <div class="row">
        <div class="col-md-6">
            <div class="input-container">
                <label><i class="fa fa-lock green-text" aria-hidden="true"></i> &nbsp;&nbsp;
                    Password</label>
                <input type="password"
                       name="password"
                       class="input-element"/>
                <span class="input-error"></span>
            </div>
        </div>

        <div class="col-md-6">
            <div class="input-container">
                <label><i class="fa fa-lock green-text" aria-hidden="true"></i> &nbsp;&nbsp; Confirm
                    Password</label>
                <input type="password"
                       name="confirm_password"
                       class="input-element"/>
                <span class="input-error"></span>
            </div>
        </div>
    </div>

    <hr/>

    <div class="row">


        <div class="col-md-12">
            {!! Form::submit('Register', array('class' => 'action-btn btn-main')) !!}
        </div>

    </div>

{!! Form::close() !!}