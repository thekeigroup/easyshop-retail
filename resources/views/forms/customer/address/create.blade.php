@extends('customer._customer')
@section('content')
    {!! Form::open(array('route'=> 'customer.address.store', 'method'=>'POST')) !!}
    @include('forms.customer.address.form')
    {!! Form::submit('Save', array('class' => 'action-btn btn-main')) !!}
    {!! Form::close() !!}
@endsection