<div class="row">
    <div class="col-xs-6 col-md-6 col-lg-6">
        <div>
            <span>Shipping Address</span>
            <hr>
            @if(empty($shippingAddress->first_name))
                No Shipping Address Added
            @else
                <p>{{$shippingAddress->first_name}} {{$shippingAddress->last_name}}</p>
                <p>{{$shippingAddress->address1}}</p>
                <p>{{$shippingAddress->phone}}</p>
            @endif
        </div>
    </div>
    <div class="col-xs-6 col-md-6 col-lg-6">
        <div>
            <span>Billing Address</span>
            <hr>
            @if(empty($billingAddress->first_name))
                No Billing Address Added
            @else
                <p>{{$billingAddress->first_name}} {{$billingAddress->last_name}}</p>
                <p>{{$billingAddress->address1}}</p>
                <p>{{$billingAddress->phone}}</p>
            @endif
        </div>
    </div>
</div>