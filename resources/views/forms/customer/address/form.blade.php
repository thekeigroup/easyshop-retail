<div class="row">
    <div class="col-md-6">

        <div class="row">

            <div class="col-md-6">
                <div class="input-container">
                    <label>First Name*</label>
                    <input type="text"
                           name="first_name"
                           class="input-element" value="{{$address->first_name}}"/> <!-- if the input field has an error add class : error -->
                    <span class="input-error"></span> <!-- if the input field has an error add text in this span -->
                </div>
            </div>

            <div class="col-md-6">
                <div class="input-container">
                    <label>Last Name*</label>
                    <input type="text"
                           name="last_name"
                           class="input-element" value="{{$address->last_name}}"/>
                    <span class="input-error"></span>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-md-6">
                <div class="input-container">
                    <label>Main Address</label>
                    <input type="text"
                           name="address1"
                           class="input-element" value="{{$address->address1}}"/> <!-- if the input field has an error add class : error -->
                    <span class="input-error"></span> <!-- if the input field has an error add text in this span -->
                </div>
            </div>

            <div class="col-md-6">
                <div class="input-container">
                    <label>Alternate Address</label>
                    <input type="text"
                           name="address2"
                           class="input-element"/>
                    <span class="input-error"></span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-container">
                    <label>Telephone*</label>
                    <input type="text"
                           name="phone" value="{{$address->phone}}"
                           class="input-element"/>
                    <span class="input-error"></span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-container">
                    <label>Alternate Telephone</label>
                    <input type="text"
                           name="name_4"
                           class="input-element"/>
                    <span class="input-error"></span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-container">
                    <label>Address Type</label>
                    <select name="type" value="{{$address->type}}">
                        <option value="SHIPPING">Shipping</option>
                        <option value="BILLING">Billing</option>
                        <option></option>
                    </select>
                </div>
            </div>

        </div>

    </div>
</div>

