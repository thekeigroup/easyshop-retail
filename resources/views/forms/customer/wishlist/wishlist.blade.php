<div class="row">
    @if(count($wishlists) <= 0)
        <center>
            <span>No products added to your wishlist</span>
        </center>
    @else
        <table>
            @foreach($wishlists as $wishlist )
                <tr>
                    <td>
                        {{$wishlist->product->product_name}}
                    </td>
                    <td>

                    </td>
                    <td>
                        <a href="{{route('customer.wishlist.destroy', $wishlist->id)}}">Remove</a>
                    </td>
                </tr>
            @endforeach
        </table>
    @endif
</div>