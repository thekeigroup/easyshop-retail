<div class="row">
    <h2>Order Items Info</h2>
    <div class="col-md-12">
        <table class="table table-responsive">
            <thead>
            <tr>
                <th>Product</th>
                <th>Qty</th>
                <th>Price</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
    <hr/>
    <h2>Order Address Info</h2>
    <div class="row">
        <div class="col-md-6">
            <h3>Billing Address</h3>
        </div>

        <div class="col-md-6">
            <h3>Shipping Address</h3>
        </div>
    </div>
    <hr/>
    <h2>Order Info</h2>
    <div class="col-md-12">
        <table class="table table-responsive">
            <thead>
            <tr>
                <th>Transaction No.</th>
                <th>Shipping Option</th>
                <th>Payment Option</th>
            </tr>
            </thead>
        </table>
    </div>
</div>