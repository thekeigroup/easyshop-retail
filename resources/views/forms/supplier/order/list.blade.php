<div class="row">
    <div class="col-lg-12">
        @if(count($orders) <= 0)
            No orders found
        @else
            <table class="table table-responsive">
                <thead>
                <tr>
                    <td>Order Date</td>
                    <td>Shipping Option</td>
                    <td>Payment Option</td>
                    <td>Order Status</td>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td>{{$order->created_date}}</td>
                        <td>{{$order->shipping_option}}</td>
                        <td>{{$order->payment_option}}</td>
                        <td>{{$order->order_status}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    </div>
</div>