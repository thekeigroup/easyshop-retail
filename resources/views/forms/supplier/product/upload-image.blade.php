@extends('supplier._supplier')
@section('content-title')
    Upload images for {{$product->product_name}}
@endsection
@section('content')
    @include('flash-messages')
    {!! Form::open(array('route' => 'supplier.product.store-image', 'files' => true, 'method' => 'POST')) !!}
    <input type="hidden" name="product_id" value="{{$product->id}}">
    <div class="row">
        <div class="col-md-6">
            <div class="input-container">
                <label>Product Name</label>
                <input type="file" name="product_image" class="input-element"/>
                <!-- if the input field has an error add class : error -->
                <span class="input-error"></span> <!-- if the input field has an error add text in this span -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="input-container">
                <label>Main Image?</label>
                <input type="checkbox" name="is_main_image" class="input-element"/>
            </div>
        </div>

    </div>
    {!! Form::submit('Upload Product Image', array('class' => 'action-btn btn-main')) !!}
@endsection