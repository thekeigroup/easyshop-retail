<div class="row">
    <div class="col-md-6">
        <section class="section register inner-left-xs">
            <h2 class="bordered">Add Product</h2>
            <p>Become a supplier today</p>

            {!! Form::model($product,['route'=>['product.update',$product->id], 'class' => 'register-form cf-style-1'])) !!}
                @include('forms.product.form', ['submitTxtBtn'=>'Update'])
            {!! Form::close() !!}
    </div><!-- /.col -->
</div>
{!! Form::close() !!}