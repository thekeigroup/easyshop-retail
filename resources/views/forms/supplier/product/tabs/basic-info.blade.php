<div class="row">
    <div class="col-md-6">
        <div class="input-container">
            <label>Product Name</label>
            <input type="text"
                   name="product_name"
                   class="input-element"/> <!-- if the input field has an error add class : error -->
            <span class="input-error"></span> <!-- if the input field has an error add text in this span -->
        </div>
    </div>
    <div class="col-md-6">
        <div class="input-container">
            <label>Slug</label>
            <input type="text"
                   name="slug"
                   class="input-element"/>
            <span class="input-error"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="input-container">
            <label>Category</label>
            <input type="text"
                   name="category"
                   class="input-element"/>
            <span class="input-error"></span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="input-container">
            <label>SKU</label>
            <input type="text"
                   name="sku"
                   class="input-element"/>
            <span class="input-error"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="input-container">
            <label>In Stock?</label>
            <input type="checkbox" name="in_stock">
            <span class="input-error"></span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="input-container">
            <label>Quantity</label>
            <input type="text" name="qty" class="input-element">
            <span class="input-error"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="input-container">
            <label>Price</label>
            <input type="text" name="price" class="input-element">
            <span class="input-error"></span>
        </div>
    </div><div class="col-md-6">
        <div class="input-container">
            <label>Track Stock?</label>
            <input type="checkbox" name="track_stock">
            <span class="input-error"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="input-container">
            <label>Description</label>
            <textarea class="input-element" name="description"></textarea>
            <span class="input-error"></span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="input-container">
            <label>Publish?</label>
            <input type="checkbox" name="published">
            <span class="input-error"></span>
        </div>
    </div>
</div>


