@extends('supplier._supplier')
@section('content')
    <div class="row">
        {!! Form::open(array('route' => 'supplier.product.store', 'method'=>'POST')) !!}
        <div class="col-lg-12">
            @include('forms.supplier.product.tabs.basic-info')
            {!! Form::submit('Create & Continue',array('class' => 'action-btn btn-main')) !!}
        </div>
        {!! Form::close() !!}
    </div>
@endsection