<a href="{{URL::route('supplier.product.create')}}">Add Product</a>
<br/>
@if($products->isEmpty())
    <p> No products found</p>
@else
    <table class="table table-responsive" style="width: 100%;">
        <thead>
        <tr>
            <td>Name</td>
            <td>Price</td>
            <td>Image</td>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <td>{{$product->product_name}}</td>
                <td>{{$product->price}}</td>
                @foreach($product->images as $image)
                    <td>
                        <img height="100" width="100"
                             src="{{asset('storage/'.$image->where('is_main_image', '=', 1)->first()->path)}}">
                    </td>
                @endforeach()
            </tr>
        @endforeach()
        </tbody>
    </table>
@endif



