<div class="field-row">
    <label>Product Name*</label>
    <input type="text" class="le-input" name="product_name">
</div><!-- /.field-row -->
<div class="field-row">
    <label>Description*</label>
    <textarea class="le-input" name="description"></textarea>
</div><!-- /.field-row -->
<div class="field-row">
    <label>Price*</label>
    <input type="text" class="le-input" name="price"></textarea>
</div><!-- /.field-row -->
<div class="field-row">
    <label>Product Image</label>
    <input type="file" name="product_image">
</div>
<div class="field-row">
    <label>Category*</label>
    <select name="category_id" class="form-control">
        <option value="Individual">Category</option>
        <option value="Company">Company</option>
    </select>
</div><!-- /.field-row -->
<div class="field-row">
    <label>Brand*</label>
    <select name="supplier_type" class="form-control">
        <option value="Individual">Brand</option>
        <option value="Company">Company</option>
    </select>
</div><!-- /.field-row -->

<div class="buttons-holder">
    {!! Form::submit($submitTxtBtn , array('class'=>'le-button huge')) !!}
</div><!-- /.buttons-holder -->