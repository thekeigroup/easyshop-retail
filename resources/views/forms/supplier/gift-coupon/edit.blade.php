@extends('supplier._supplier')
@section('content')
    {!! Form::open(array('route' => array('supplier.gift-coupon.update', $giftCoupon->id), 'method' => 'PUT')) !!}
    @include('forms.supplier.gift-coupon.form')
    {!! Form::submit('Save', array('class' => 'action-btn btn-main')) !!}
    {!! Form::close() !!}
@endsection