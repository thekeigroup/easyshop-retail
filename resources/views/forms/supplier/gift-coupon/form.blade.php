<div class="row">
    <div class="col-md-6">
        <div class="input-container">
            <label>Name</label>
            <input type="text" name="name" class="input-element" value="{{$giftCoupon->name}}"/> <!-- if the input field has an error add class : error -->
            <span class="input-error"></span> <!-- if the input field has an error add text in this span -->
        </div>
    </div>
    <div class="col-md-6">
        <div class="input-container">
            <label>Code</label>
            <input type="text" name="code" class="input-element" value="{{$giftCoupon->code}}"/>
            <span class="input-error"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="input-container">
            <label>Discount</label>
            <input type="text" name="discount" class="input-element" value="{{$giftCoupon->discount}}"/>
            <span class="input-error"></span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="input-container">
            <label>Start Date</label>
            <input type="date" name="start_date" class="input-element" value="{{$giftCoupon->start_date}}"/>
            <span class="input-error"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="input-container">
            <label>End Date</label>
            <input type="date" name="end_date" class="input-element" value="{{$giftCoupon->end_date}}">
            <span class="input-error"></span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="input-container">
            <label>Status</label>
            <select name="status" class="input-element" >
                <option value="ENABLED">Enabled</option>
                <option value="DISABLED">Disabled</option>
            </select>
            <span class="input-error"></span>
        </div>
    </div>
</div>


