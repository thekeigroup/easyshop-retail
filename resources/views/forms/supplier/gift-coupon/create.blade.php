@extends('supplier._supplier')
@section('content')
    {!! Form::open(array('route' => 'supplier.gift-coupon.store')) !!}
    @include('forms.supplier.gift-coupon.form')
    {!! Form::submit('Save', array('class' => 'action-btn btn-main')) !!}&nbsp;
    <a href="{{route('supplier.gift-coupon.index')}}" class="btn-default">Cancel</a>
    {!! Form::close() !!}
@endsection
