<div class="row">
    <a href="{{URL::route('supplier.gift-coupon.create')}}">Create Gift Coupon</a>
    <div class="col-lg-12">
        <table class="table table-responsive ">
            <thead>
            <tr>
                <th>Name</th>
                <th>Coupon Code</th>
                <th>Status</th>
                <th>Start Date</th>
                <th>End Date</th>
            </tr>
            </thead>
            <tbody>
            @if(count($giftCoupons) <= 0)
                No gift coupons created
            @else
                @foreach($giftCoupons as $giftCoupon)
                    <tr>
                        <td>{{$giftCoupon->name}}</td>
                        <td>{{$giftCoupon->code}}</td>
                        <td>{{$giftCoupon->status}}</td>
                        <td>{{$giftCoupon->start_date}}</td>
                        <td>{{$giftCoupon->end_date}}</td>
                        <td>
                            <a href="{{route('supplier.gift-coupon.edit' ,$giftCoupon->id )}}">Edit</a> |
                            <a href="{{route('supplier.gift-coupon.delete', $giftCoupon->id)}}">Delete</a>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>