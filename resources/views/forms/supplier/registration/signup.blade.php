{!! Form::open(array('route' => 'register-supplier')) !!}
<div class="row">
    <div class="col-md-6">
        <div class="input-container">
            <label>First Name</label>
            <input type="text" name="first_name" class="input-element"/>
            <!-- if the input field has an error add class : error -->
            <span class="input-error"></span> <!-- if the input field has an error add text in this span -->
        </div>
    </div>
    <div class="col-md-6">
        <div class="input-container">
            <label>Last Name</label>
            <input type="text" name="last_name" class="input-element"/>
            @if($errors->has('last_name'))<span class="input-error" >{{$errors->first('last_name')}}</span>@endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="input-container">
            <label>Preferred Name</label>
            <input type="text" name="preferred_name" class="input-element"/>
            <span class="input-error"></span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="input-container">
            <label>Telephone</label>
            <input type="text"
                   name="mobile_phone_no"
                   class="input-element"/>
            <span class="input-error"></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="input-container">
            <label>Supplier Type</label>
            <select class="input-element" name="supplier_type">
                <option value="COMPANY">Company</option>
                <option value="INDIVIDUAL">Individual</option>
            </select>
            <span class="input-error"></span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="input-container">
            <label>Email</label>
            <input type="email" name="email" class="input-element"/>
            <span class="input-error"></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="input-container">
            <label><i class="fa fa-lock green-text" aria-hidden="true"></i> &nbsp;&nbsp; Password</label>
            <input type="password" name="password" class="input-element"/>
            <span class="input-error"></span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="input-container">
            <label><i class="fa fa-lock green-text" aria-hidden="true"></i> &nbsp;&nbsp; Confirm Password</label>
            <input type="password" name="confirm_password" class="input-element"/>
            <span class="input-error"></span>
        </div>
    </div>
</div>
{!! Form::submit('Register', array('class' => 'action-btn btn-main')) !!}
{!! Form::close() !!}