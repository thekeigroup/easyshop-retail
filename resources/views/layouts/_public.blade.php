<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title') | Easy Shop Retail</title>

        <!-- Styles -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/css/datepicker.css" />

        <link href="{{asset('css/admin/master.css')}}" rel="stylesheet" >

        <!-- Font awesome -->
        <script src="https://use.fontawesome.com/7fcf9fe887.js"></script>

    </head>
    <body>

        <div id="wrapper">

            <!-- Start Header partial -->

            <header id="main-header">

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="logo">
                                <img src="images/logo-placeholder.svg" />
                            </div>
                        </div>
                    </div>
                </div>

            </header>

            <!-- End Header partial -->

            <!-- Start Nav partial -->

            <div id="menu-bar">
                <i class="fa fa-bars" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;
                MENU
            </div>

            <nav id="main-nav">
                @yield('backend-menu')
            </nav>

            <!-- End Nav partial -->

            <div class="section">

                <div class="container-fluid">

                    <div class="content center-piece wide-width left-bar">

                        <h4>Content Title</h4>

                    </div>

                </div>
            </div>

            <!-- @endsection -->


            <!-- End Registration partial -->

        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="{{asset('js/admin/master.js')}}"></script>

    </body>
</html>

