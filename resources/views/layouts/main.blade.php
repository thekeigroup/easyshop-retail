@include('layouts.head')
<div class="wrapper">

    @yield('top-bar')

    @yield('header')

    @yield('top-banner-and-menu')
    
    @yield('home-banners')
    
    @yield('products-tab')
    
    @yield('content')

    @yield('footer')
</div>
@include('layouts.foot')