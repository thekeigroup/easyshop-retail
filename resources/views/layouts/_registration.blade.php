<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title') | Easy Shop Retail</title>
    <!-- Styles -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/css/datepicker.css"/>
    <link href="{{asset('css/admin/master.css')}}" rel="stylesheet">
    <!-- Font awesome -->
    <script src="https://use.fontawesome.com/7fcf9fe887.js"></script>

</head>
<body>

<div id="wrapper">

    <!-- Start Header partial -->

    <header id="main-header">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div id="logo">
                        <img src="images/logo-placeholder.svg"/>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- End Header partial -->
    <div>
        @include('flash-messages')
        <div class="container-fluid section-header">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-text align-center">
                        <h4>@yield('header-text')</h4>
                        <span class="descriptive-text">
                            @yield('descriptive-text')
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="content center-piece medium-width shadow left-bar">

            <div class="container-fluid">
                @yield('content')
            </div>

        </div>
    </div>

    <!-- End Registration partial -->

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/js/datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/js/i18n/datepicker.en.min.js"></script>

</body>
</html>
