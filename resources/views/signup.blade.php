@extends('layouts._registration')
@section('title')
    Customer Account Registration
@endsection
@section('header-text')
    Create New Customer Account
@endsection
@section('descriptive-text')
    Please fill form below
@endsection
@section('content')
    @include('forms.customer.registration.signup')
@endsection()