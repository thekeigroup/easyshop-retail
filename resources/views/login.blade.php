@extends('layouts.main')
@section('top-bar')
@include('includes.top-bar-nav')
@endsection()
@section('header')
@include('includes.header')
@endsection()
@section('content')
<div class="col-md-6 center">
    <section class="section sign-in inner-right-xs">
        <h2 class="bordered">Sign In</h2>
        <p>Hello, Welcome to your account</p>
        @include('flash-messages')
        {!! Form::open(array('route' => 'authenticate', 'method' => 'POST')) !!}
            <div class="field-row">
                <label>Email</label>
                <input type="text" class="le-input" name="email">
            </div><!-- /.field-row -->

            <div class="field-row">
                <label>Password</label>
                <input type="password" class="le-input" name="password">
            </div><!-- /.field-row -->

            <div class="field-row clearfix">
                <span class="pull-left">
                    <label class="content-color"><input type="checkbox" class="le-checbox auto-width inline"> <span class="bold">Remember me</span></label>
                </span>
                <span class="pull-right">
                    <a href="#" class="content-color bold">Forgotten Password ?</a>
                </span>
            </div>

            <div class="buttons-holder">
                {!! Form::submit('Sign In', array('class' => 'le-button huge')) !!}
            </div><!-- /.buttons-holder -->
       {!! Form::close() !!}

    </section><!-- /.sign-in -->
</div><!-- /.col -->
@endsection()
