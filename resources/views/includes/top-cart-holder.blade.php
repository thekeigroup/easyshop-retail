<div class="basket">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">

        <div class="basket-item-count">
            @if(Session::get('cart') == null)
                <span class="count">0</span>
                <img src="{{asset('images/icon-cart.png')}}" alt=""/>
            @else
                <span class="count">{{Session::get('cart')->sum('qty')}}</span>
                <img src="{{asset('images/icon-cart.png')}}" alt=""/>
            @endif
        </div>
        <div class="total-price-basket">
            <span class="lbl">your cart:</span>
            <span class="total-price">
                @if(Session::get('cart') == null)
                    <span class="sign">GHS</span><span class="value"> 0</span>
                @else
                    <span class="sign">GHS</span><span class="value"> {{number_format(Session::get('cart')->sum('price') * Session::get('cart')->sum('qty'), 2)}}</span>
                @endif
            </span>
        </div>

    </a>

    <ul class="dropdown-menu">
        @if(Session::get('cart') == null)
            <li>
                <div class="basket-item">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4 no-margin text-center">
                            <div class="thumb">

                            </div>
                        </div>
                        <div class="col-xs-8 col-sm-8 no-margin">
                            <div class="title">No Item In Cart</div>
                            <div class="price"></div>
                        </div>
                    </div>
                </div>
            </li>
        @else
            @foreach(Session::get('cart') as $product)
                <li>
                    <div class="basket-item">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4 no-margin text-center">
                                <div class="thumb">
                                    @if(isset($product['main_image']))
                                        <img alt="" src="{{Storage::url($product['main_image'])}}"/>
                                    @else
                                        <img alt="No Image"/>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-8 col-sm-8 no-margin">
                                <div class="title">{{$product['name']}}</div>
                                <div class="price">{{$product['price']}}</div>
                            </div>
                        </div>
                        <a class="close-btn" href="#"></a>
                    </div>
                </li>
            @endforeach
            <li class="checkout">
                <div class="basket-item">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <a href="{{route('cart.view')}}" class="le-button inverse">View cart</a>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <a href="{{route('checkout')}}" class="le-button">Checkout</a>
                        </div>
                    </div>
                </div>
            </li>
        @endif
    </ul>
</div><!-- /.basket -->