<nav class="top-bar animate-dropdown">
    <div class="container">
        <div class="col-xs-12 col-sm-6 no-margin">

        </div><!-- /.col -->

        <div class="col-xs-12 col-sm-6 no-margin">
            <ul class="right">
                <li class="dropdown">

                    <a class="dropdown-toggle" data-toggle="dropdown" href="#your-account">Your Account</a>
                    <ul class="dropdown-menu" role="menu">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">My Account</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">My Orders</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">My WishList</a></li>
                        
                    </ul>
                </li>
                <li><a href="{{URL::route('signup')}}">Sign Up</a></li>
                <li><a href="{{URL::route('login')}}">Login</a></li>
            </ul>
        </div><!-- /.col -->
    </div><!-- /.container -->
</nav>

