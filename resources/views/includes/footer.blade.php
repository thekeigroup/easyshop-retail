<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="color-bg">
    <div class="container">
        <div class="row no-margin widgets-row">
            <div class="col-xs-12  col-sm-4 no-margin-left">
                <!-- ============================================================= FEATURED PRODUCTS ============================================================= -->
                <div class="widget">
                    <h2>Featured products</h2>
                    <div class="body">
                        <ul>
                            <li>
                               
                            </li>

                            <li>
                               
                            </li>

                            <li>
                               
                            </li>
                        </ul>
                    </div><!-- /.body -->
                </div> <!-- /.widget -->
                <!-- ============================================================= FEATURED PRODUCTS : END ============================================================= -->
            </div><!-- /.col -->

            <div class="col-xs-12 col-sm-4 ">
                <!-- ============================================================= ON SALE PRODUCTS ============================================================= -->
                <div class="widget">
                    <h2>On-Sale Products</h2>
                    <div class="body">
                        <ul>
                            <li>
                               

                            </li>
                            <li>
                               
                            </li>

                            <li>
                               
                            </li>
                        </ul>
                    </div><!-- /.body -->
                </div> <!-- /.widget -->
                <!-- ============================================================= ON SALE PRODUCTS : END ============================================================= -->
            </div><!-- /.col -->

            <div class="col-xs-12 col-sm-4 ">
                <!-- ============================================================= TOP RATED PRODUCTS ============================================================= -->
                <div class="widget">
                    <h2>Top Rated Products</h2>
                    <div class="body">
                        <ul>
                            <li>
                                
                            </li>

                            <li>
                               
                            </li>

                            <li>
                               
                            </li>
                        </ul>
                    </div><!-- /.body -->
                </div><!-- /.widget -->
                <!-- ============================================================= TOP RATED PRODUCTS : END ============================================================= -->
            </div><!-- /.col -->
        </div><!-- /.widgets-row-->
    </div><!-- /.container -->

    <div class="sub-form-row">
        <div class="container">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 no-padding">
                <form role="form">
                    <input placeholder="Subscribe to our newsletter">
                    <button class="le-button">Subscribe</button>
                </form>
            </div>
        </div><!-- /.container -->
    </div><!-- /.sub-form-row -->

    <div class="link-list-row">
        <div class="container no-padding">
            <div class="col-xs-12 col-md-4 ">
                <!-- ============================================================= CONTACT INFO ============================================================= -->
                <div class="contact-info">
                    <div class="footer-logo">
                       
                    </div><!-- /.footer-logo -->

                    <p class="regular-bold"> Feel free to contact us via phone,email or just send us mail.</p>

                    <p>
                        info@keigroup.com

                    </p>

                    <div class="social-icons">
                        <h3>Get in touch</h3>
                        <ul>
                            <li><a href="http://facebook.com/transvelo" class="fa fa-facebook"></a></li>
                            <li><a href="#" class="fa fa-twitter"></a></li>
                            <li><a href="#" class="fa fa-pinterest"></a></li>
                            <li><a href="#" class="fa fa-linkedin"></a></li>
                            <li><a href="#" class="fa fa-stumbleupon"></a></li>
                            <li><a href="#" class="fa fa-dribbble"></a></li>
                            <li><a href="#" class="fa fa-vk"></a></li>
                        </ul>
                    </div><!-- /.social-icons -->

                </div>
                <!-- ============================================================= CONTACT INFO : END ============================================================= -->
            </div>

            <div class="col-xs-12 col-md-8 no-margin">
                <!-- ============================================================= LINKS FOOTER ============================================================= -->
                <div class="link-widget">
                    <div class="widget">
                        <h3>Find it fast</h3>
                        <ul>
                            <li><a href="category-grid.html">laptops &amp; computers</a></li>
                            <li><a href="category-grid.html">Cameras &amp; Photography</a></li>
                            <li><a href="category-grid.html">Smart Phones &amp; Tablets</a></li>
                            <li><a href="category-grid.html">Video Games &amp; Consoles</a></li>
                            <li><a href="category-grid.html">TV &amp; Audio</a></li>
                            <li><a href="category-grid.html">Gadgets</a></li>
                            <li><a href="category-grid.html">Car Electronic &amp; GPS</a></li>
                            <li><a href="category-grid.html">Accesories</a></li>
                        </ul>
                    </div><!-- /.widget -->
                </div><!-- /.link-widget -->

                <div class="link-widget">
                    <div class="widget">
                        <h3>Information</h3>
                        <ul>
                            <li><a href="category-grid.html">Find a Store</a></li>
                            <li><a href="category-grid.html">About Us</a></li>
                            <li><a href="category-grid.html">Contact Us</a></li>
                            <li><a href="category-grid.html">Weekly Deals</a></li>
                            <li><a href="category-grid.html">Gift Cards</a></li>
                            <li><a href="{{URL::route('become-supplier')}}">Become A Supplier</a></li>

                        </ul>
                    </div><!-- /.widget -->
                </div><!-- /.link-widget -->

                <div class="link-widget">
                    <div class="widget">
                        <h3>Information</h3>
                        <ul>
                            <li><a href="category-grid.html">My Account</a></li>
                            <li><a href="category-grid.html">Order Tracking</a></li>
                            <li><a href="category-grid.html">Wish List</a></li>
                            <li><a href="category-grid.html">Customer Service</a></li>
                            <li><a href="category-grid.html">Returns / Exchange</a></li>
                            <li><a href="category-grid.html">FAQs</a></li>
                            <li><a href="category-grid.html">Product Support</a></li>
                            <li><a href="category-grid.html">Extended Service Plans</a></li>
                        </ul>
                    </div><!-- /.widget -->
                </div><!-- /.link-widget -->
                <!-- ============================================================= LINKS FOOTER : END ============================================================= -->
            </div>
        </div><!-- /.container -->
    </div><!-- /.link-list-row -->

    <div class="copyright-bar">
        <div class="container">
            <div class="col-xs-12 col-sm-6 no-margin">
                <div class="copyright">
                    &copy; <a href="index-2.html">Keigroup</a> - all rights reserved
                </div><!-- /.copyright -->
            </div>
            <div class="col-xs-12 col-sm-6 no-margin">
                <div class="payment-methods ">
                    <ul>
                        <li><img alt="" src="assets/images/payments/payment-visa.png"></li>
                        <li><img alt="" src="assets/images/payments/payment-master.png"></li>
                        <li><img alt="" src="assets/images/payments/payment-paypal.png"></li>
                        <li><img alt="" src="assets/images/payments/payment-skrill.png"></li>
                    </ul>
                </div><!-- /.payment-methods -->
            </div>
        </div><!-- /.container -->
    </div><!-- /.copyright-bar -->
</footer><!-- /#footer -->
<!-- ============================================================= FOOTER : END ============================================================= -->