<header>
    <div class="container no-padding">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
            <!-- ============================================================= LOGO ============================================================= -->
            <div class="logo">
                <a href="{{URL::route('home')}}">
                    <img alt="logo" src="{{asset('images/logo.jpg')}}" width="500" height="200" style="margin-top: -60px;"/>
                </a>
            </div><!-- /.logo -->
            <!-- ============================================================= LOGO : END ============================================================= -->
        </div><!-- /.logo-holder -->

        <div class="col-xs-12 col-sm-12 col-md-6 top-search-holder no-margin">
            @include('includes.search')
        </div><!-- /.top-search-holder -->

        <div class="col-xs-12 col-sm-12 col-md-3 top-cart-row no-margin">
            <div class="top-cart-row-container">
                <div class="wishlist-compare-holder">
                    @yield('wishlist-compare-holder')
                </div>
                <div class="top-cart-holder dropdown animate-dropdown">
                    @include('includes.top-cart-holder')
                </div><!-- /.top-cart-holder -->
            </div>
        </div>
    </div>
</header>
