<div class="tab-pane" id="additional-info">
    <ul class="tabled-data">
        <li>
            <label>weight</label>
            <div class="value">7.25 kg</div>
        </li>
        <li>
            <label>dimensions</label>
            <div class="value">90x60x90 cm</div>
        </li>
        <li>
            <label>size</label>
            <div class="value">one size fits all</div>
        </li>
        <li>
            <label>color</label>
            <div class="value">white</div>
        </li>
        <li>
            <label>guarantee</label>
            <div class="value">5 years</div>
        </li>
    </ul><!-- /.tabled-data -->

    <div class="meta-row">
        <div class="inline">
            <label>SKU:</label>
            <span>54687621</span>
        </div><!-- /.inline -->

        <span class="seperator">/</span>

        <div class="inline">
            <label>categories:</label>
            <span><a href="#">-50% sale</a>,</span>
            <span><a href="#">gaming</a>,</span>
            <span><a href="#">desktop PC</a></span>
        </div><!-- /.inline -->

        <span class="seperator">/</span>

        <div class="inline">
            <label>tag:</label>
            <span><a href="#">fast</a>,</span>
            <span><a href="#">gaming</a>,</span>
            <span><a href="#">strong</a></span>
        </div><!-- /.inline -->
    </div><!-- /.meta-row -->
</div><!-- /.tab-pane #additional-info -->