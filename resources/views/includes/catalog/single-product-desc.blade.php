<div class="tab-pane active" id="description">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sit amet porttitor eros. Praesent quis diam placerat, accumsan velit interdum, accumsan orci. Nunc libero sem, elementum in semper in, sollicitudin vitae dolor. Etiam sed tempus nisl. Integer vel diam nulla. Suspendisse et aliquam est. Nulla molestie ante et tortor sollicitudin, at elementum odio lobortis. Pellentesque neque enim, feugiat in elit sed, pharetra tempus metus. Pellentesque non lorem nunc. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>

    <p>Sed consequat orci vel rutrum blandit. Nam non leo vel risus cursus porta quis non nulla. Vestibulum vitae pellentesque nunc. In hac habitasse platea dictumst. Cras egestas, turpis a malesuada mollis, magna tortor scelerisque urna, in pellentesque diam tellus sit amet velit. Donec vel rhoncus nisi, eget placerat elit. Phasellus dignissim nisl vel lectus vehicula, eget vehicula nisl egestas. Duis pretium sed risus dapibus egestas. Nam lectus felis, sodales sit amet turpis se.</p>

    <div class="meta-row">
        <div class="inline">
            <label>SKU:</label>
            <span>54687621</span>
        </div><!-- /.inline -->

        <span class="seperator">/</span>

        <div class="inline">
            <label>categories:</label>
            <span><a href="#">-50% sale</a>,</span>
            <span><a href="#">gaming</a>,</span>
            <span><a href="#">desktop PC</a></span>
        </div><!-- /.inline -->

        <span class="seperator">/</span>

        <div class="inline">
            <label>tag:</label>
            <span><a href="#">fast</a>,</span>
            <span><a href="#">gaming</a>,</span>
            <span><a href="#">strong</a></span>
        </div><!-- /.inline -->
    </div><!-- /.meta-row -->
</div><!-- /.tab-pane #description -->