@extends('layouts.category-grid')
@section('content')
    <div id="single-product">
        <div class="container">

            <div class="no-margin col-xs-12 col-sm-6 col-md-5 gallery-holder">
                <div class="product-item-holder size-big single-product-gallery small-gallery">

                    <div id="owl-single-product" class="owl-carousel">
                        <div class="single-product-gallery-item" id="slide1">
                            <a data-rel="prettyphoto" href="assets/images/products/product-gallery-01.jpg">
                                <img class="img-responsive" alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-gallery-01.jpg" />
                            </a>
                        </div><!-- /.single-product-gallery-item -->

                        <div class="single-product-gallery-item" id="slide2">
                            <a data-rel="prettyphoto" href="assets/images/products/product-gallery-01.jpg">
                                <img class="img-responsive" alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-gallery-01.jpg" />
                            </a>
                        </div><!-- /.single-product-gallery-item -->

                        <div class="single-product-gallery-item" id="slide3">
                            <a data-rel="prettyphoto" href="assets/images/products/product-gallery-01.jpg">
                                <img class="img-responsive" alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-gallery-01.jpg" />
                            </a>
                        </div><!-- /.single-product-gallery-item -->
                    </div><!-- /.single-product-slider -->


                    <div class="single-product-gallery-thumbs gallery-thumbs">

                        <div id="owl-single-product-thumbnails" class="owl-carousel">
                            <a class="horizontal-thumb active" data-target="#owl-single-product" data-slide="0" href="#slide1">
                                <img width="67" alt="" src="assets/images/blank.gif" data-echo="assets/images/products/gallery-thumb-01.jpg" />
                            </a>

                            <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="1" href="#slide2">
                                <img width="67" alt="" src="assets/images/blank.gif" data-echo="assets/images/products/gallery-thumb-01.jpg" />
                            </a>

                            <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="2" href="#slide3">
                                <img width="67" alt="" src="assets/images/blank.gif" data-echo="assets/images/products/gallery-thumb-01.jpg" />
                            </a>

                            <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="0" href="#slide1">
                                <img width="67" alt="" src="assets/images/blank.gif" data-echo="assets/images/products/gallery-thumb-01.jpg" />
                            </a>

                            <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="1" href="#slide2">
                                <img width="67" alt="" src="assets/images/blank.gif" data-echo="assets/images/products/gallery-thumb-01.jpg" />
                            </a>

                            <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="2" href="#slide3">
                                <img width="67" alt="" src="assets/images/blank.gif" data-echo="assets/images/products/gallery-thumb-01.jpg" />
                            </a>

                            <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="0" href="#slide1">
                                <img width="67" alt="" src="assets/images/blank.gif" data-echo="assets/images/products/gallery-thumb-01.jpg" />
                            </a>

                            <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="1" href="#slide2">
                                <img width="67" alt="" src="assets/images/blank.gif" data-echo="assets/images/products/gallery-thumb-01.jpg" />
                            </a>

                            <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="2" href="#slide3">
                                <img width="67" alt="" src="assets/images/blank.gif" data-echo="assets/images/products/gallery-thumb-01.jpg" />
                            </a>
                        </div><!-- /#owl-single-product-thumbnails -->

                        <div class="nav-holder left hidden-xs">
                            <a class="prev-btn slider-prev" data-target="#owl-single-product-thumbnails" href="#prev"></a>
                        </div><!-- /.nav-holder -->

                        <div class="nav-holder right hidden-xs">
                            <a class="next-btn slider-next" data-target="#owl-single-product-thumbnails" href="#next"></a>
                        </div><!-- /.nav-holder -->

                    </div><!-- /.gallery-thumbs -->

                </div><!-- /.single-product-gallery -->
            </div><!-- /.gallery-holder -->
            <div class="no-margin col-xs-12 col-sm-7 body-holder">
                <div class="body">
                    <div class="star-holder inline"><div class="star" data-score="4"></div></div>
                    <div class="availability"><label>Availability:</label><span class="available">  in stock</span></div>

                    <div class="title"><a href="#">{{$product->product_name}}</a></div>
                    <div class="brand">sony</div>

                    <div class="social-row">
                        <span class="st_facebook_hcount"></span>
                        <span class="st_twitter_hcount"></span>
                        <span class="st_pinterest_hcount"></span>
                    </div>

                    <div class="buttons-holder">
                        <a class="btn-add-to-wishlist" href="{{route('wishlist.add', $product->slug)}}">add to wishlist</a>
                        {{--<a class="btn-add-to-compare" href="#">add to compare list</a>--}}
                    </div>

                    <div class="excerpt">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ornare turpis non risus semper dapibus. Quisque eu vehicula turpis. Donec sodales lacinia eros, sit amet auctor tellus volutpat non.</p>
                    </div>

                    <div class="prices">
                        <div class="price-current">$1740.00</div>
                        <div class="price-prev">$2199.00</div>
                    </div>

                    {!! Form::open(array('route' => 'cart.add-to-cart', 'method' => 'POST')) !!}
                    <input type="hidden" name="slug" value="{{$product->slug}}" />
                    <div class="qnt-holder">
                        <div class="le-quantity">
                            <form>
                                <a class="minus" href="#reduce"></a>
                                <input name="quantity" readonly="readonly" type="text" value="1" />
                                <a class="plus" href="#add"></a>
                            </form>
                        </div>
                        <button type="submit" id="addto-cart"  href="{{route('cart.add-to-cart')}}" class="le-button huge">
                            add to cart
                        </button>
                        {{--<a id="addto-cart"></a>--}}
                    </div><!-- /.qnt-holder -->
                    {!! Form::close() !!}
                </div><!-- /.body -->

            </div><!-- /.body-holder -->
        </div><!-- /.container -->
    </div><!-- /.single-product -->

    @include('includes.catalog.single-product-tab')
@endsection
