<section id="category-grid">
    <div class="container">
        <!-- ========================================= SIDEBAR ========================================= -->
        <div class="col-xs-12 col-sm-3 no-margin sidebar narrow">
            <!-- ========================================= PRODUCT FILTER ========================================= -->
            <div class="widget">
                <h1>Product Filters</h1>
                <div class="body bordered">

                    <div class="category-filter">
                        <h2>Brands</h2>
                        <hr>
                        <ul>
                            <li><input checked="checked" class="le-checkbox" type="checkbox"  /> <label>Samsung</label> <span class="pull-right">(2)</span></li>
                            <li><input  class="le-checkbox" type="checkbox" /> <label>Dell</label> <span class="pull-right">(8)</span></li>
                            <li><input  class="le-checkbox" type="checkbox" /> <label>Toshiba</label> <span class="pull-right">(1)</span></li>
                            <li><input  class="le-checkbox" type="checkbox" /> <label>Apple</label> <span class="pull-right">(5)</span></li>
                        </ul>
                    </div><!-- /.category-filter -->

                    <div class="price-filter">
                        <h2>Price</h2>
                        <hr>
                        <div class="price-range-holder">

                            <input type="text" class="price-slider" value="" >

                            <span class="min-max">
                                Price: $89 - $2899
                            </span>
                            <span class="filter-button">
                                <a href="#">Filter</a>
                            </span>
                        </div>
                    </div><!-- /.price-filter -->

                </div><!-- /.body -->
            </div><!-- /.widget -->
            <!-- ========================================= PRODUCT FILTER : END ========================================= -->

            <!-- ========================================= CATEGORY TREE ========================================= -->
            <div class="widget accordion-widget category-accordions">
                <h1 class="border">Category Tree</h1>
                <div class="accordion">
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" href="#collapseOne">
                                laptops &amp; computers
                            </a>
                        </div>
                        <div id="collapseOne" class="accordion-body collapse in">
                            <div class="accordion-inner">
                                <ul>
                                    <li>
                                        <div class="accordion-heading">
                                            <a href="#collapseSub1" data-toggle="collapse">laptop</a>
                                        </div>
                                        <div id="collapseSub1" class="accordion-body collapse in">
                                            <ul>
                                                <li><a href="#">Two Level Accordion</a></li>
                                            </ul>
                                        </div><!-- /.accordion-body -->
                                    </li>
                                    <li>
                                        <div class="accordion-heading">
                                            <a href="#collapseSub2" data-toggle="collapse">tablet</a>
                                        </div>
                                        <div id="collapseSub2" class="accordion-body collapse in">
                                            <ul>
                                                <li>
                                                    <div class="accordion-heading">
                                                        <a href="#collapseSub3" data-toggle="collapse">Three Level Accordion</a>
                                                    </div>
                                                    <div id="collapseSub3" class="accordion-body collapse in">
                                                        <ul>
                                                            <li><a href="#">PDA</a></li>
                                                            <li><a href="#">notebook</a></li>
                                                            <li><a href="#">mini notebook</a></li>
                                                        </ul>
                                                    </div><!-- /.accordion-body -->
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li><a href="#">PDA</a></li>
                                    <li><a href="#">notebook</a></li>
                                    <li><a href="#">mini notebook</a></li>
                                </ul>
                            </div><!-- /.accordion-inner -->
                        </div>
                    </div><!-- /.accordion-group -->

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseTwo">
                                desktop PC
                            </a>
                        </div>
                        <div id="collapseTwo" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <ul>
                                    <li><a href="#">gaming</a></li>
                                    <li><a href="#">office</a></li>
                                    <li><a href="#">kids</a></li>
                                    <li><a href="#">for women</a></li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- /.accordion-group -->


                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle collapsed" data-toggle="collapse"  href="#collapse3">
                                laptops
                            </a>
                        </div>
                        <div id="collapse3" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <ul>
                                    <li><a href="#">light weight</a></li>
                                    <li><a href="#">wide monitor</a></li>
                                    <li><a href="#">ultra laptop</a></li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- /.accordion-group -->


                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle collapsed" data-toggle="collapse"  href="#collapse4">
                                notebooks
                            </a>
                        </div>
                        <div id="collapse4" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <ul>
                                    <li><a href="#">light weight</a></li>
                                    <li><a href="#">wide monitor</a></li>
                                    <li><a href="#">ultra laptop</a></li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- /.accordion-group -->

                </div><!-- /.accordion -->
            </div><!-- /.category-accordions -->
            <!-- ========================================= CATEGORY TREE : END ========================================= -->

            <!-- ========================================= LINKS ========================================= -->
            <div class="widget">
                <h1 class="border">information</h1>
                <div class="body">
                    <ul class="le-links">
                        <li><a href="#">delivery</a></li>
                        <li><a href="#">secure payment</a></li>
                        <li><a href="#">our stores</a></li>
                        <li><a href="#">contact</a></li>
                    </ul><!-- /.le-links -->
                </div><!-- /.body -->
            </div><!-- /.widget -->
            <!-- ========================================= LINKS : END ========================================= -->

            <div class="widget">
                <div class="simple-banner">
                    <a href="#"><img alt="" class="img-responsive" src="assets/images/blank.gif" data-echo="assets/images/banners/banner-simple.jpg" /></a>
                </div>
            </div>

            <!-- ========================================= FEATURED PRODUCTS ========================================= -->
            <div class="widget">
                <h1 class="border">Featured Products</h1>
                <ul class="product-list">

                    <li class="sidebar-product-list-item">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4 no-margin">
                                <a href="#" class="thumb-holder">
                                    <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-small-01.jpg" />
                                </a>
                            </div>
                            <div class="col-xs-8 col-sm-8 no-margin">
                                <a href="#">Netbook Acer </a>
                                <div class="price">
                                    <div class="price-prev">$2000</div>
                                    <div class="price-current">$1873</div>
                                </div>
                            </div>
                        </div>
                    </li><!-- /.sidebar-product-list-item -->

                    <li class="sidebar-product-list-item">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4 no-margin">
                                <a href="#" class="thumb-holder">
                                    <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-small-02.jpg" />
                                </a>
                            </div>
                            <div class="col-xs-8 col-sm-8 no-margin">
                                <a href="#">PowerShot Elph 115 16MP Digital Camera</a>
                                <div class="price">
                                    <div class="price-prev">$2000</div>
                                    <div class="price-current">$1873</div>
                                </div>
                            </div>
                        </div>
                    </li><!-- /.sidebar-product-list-item -->

                    <li class="sidebar-product-list-item">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4 no-margin">
                                <a href="#" class="thumb-holder">
                                    <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-small-03.jpg" />
                                </a>
                            </div>
                            <div class="col-xs-8 col-sm-8 no-margin">
                                <a href="#">PowerShot Elph 115 16MP Digital Camera</a>
                                <div class="price">
                                    <div class="price-prev">$2000</div>
                                    <div class="price-current">$1873</div>
                                </div>
                            </div>
                        </div>
                    </li><!-- /.sidebar-product-list-item -->

                    <li class="sidebar-product-list-item">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4 no-margin">
                                <a href="#" class="thumb-holder">
                                    <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-small-01.jpg" />
                                </a>
                            </div>
                            <div class="col-xs-8 col-sm-8 no-margin">
                                <a href="#">Netbook Acer </a>
                                <div class="price">
                                    <div class="price-prev">$2000</div>
                                    <div class="price-current">$1873</div>
                                </div>
                            </div>
                        </div>
                    </li><!-- /.sidebar-product-list-item -->

                    <li class="sidebar-product-list-item">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4 no-margin">
                                <a href="#" class="thumb-holder">
                                    <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-small-02.jpg" />
                                </a>
                            </div>
                            <div class="col-xs-8 col-sm-8 no-margin">
                                <a href="#">PowerShot Elph 115 16MP Digital Camera</a>
                                <div class="price">
                                    <div class="price-prev">$2000</div>
                                    <div class="price-current">$1873</div>
                                </div>
                            </div>
                        </div>
                    </li><!-- /.sidebar-product-list-item -->

                </ul><!-- /.product-list -->
            </div><!-- /.widget -->
            <!-- ========================================= FEATURED PRODUCTS : END ========================================= -->
        </div>
        <!-- ========================================= SIDEBAR : END ========================================= -->

        <div class="col-xs-12 col-sm-9 no-margin wide sidebar">
            <div id="grid-page-banner">
                <a href="#">
                    <img src="assets/images/banners/banner-gamer.jpg" alt="" />
                </a>
            </div>

            <section id="gaming">
                <div class="grid-list-products">
                    <h2 class="section-title">Results</h2>

                    <div class="control-bar">
                        <div id="popularity-sort" class="le-select" >
                            <select data-placeholder="sort by popularity">
                                <option value="1">1-100 players</option>
                                <option value="2">101-200 players</option>
                                <option value="3">200+ players</option>
                            </select>
                        </div>

                        <div id="item-count" class="le-select">
                            <select>
                                <option value="1">24 per page</option>
                                <option value="2">48 per page</option>
                                <option value="3">32 per page</option>
                            </select>
                        </div>
                    </div><!-- /.control-bar -->

                    <div class="tab-content">
                        <div id="grid-view" class="products-grid fade tab-pane in active">

                            <div class="product-grid-holder">
                                <div class="row no-margin">
                                   
                                    @if($products->isEmpty())
                                    <p>No results found</p>
                                    @else
                                    @foreach($products as $product)
                                    <div class="col-xs-12 col-sm-4 no-margin product-item-holder hover">
                                        <div class="product-item">
                                            <div class="image">
                                                <img alt="" src="{{asset('storage/'.$product->product_image)}}"/>
                                            </div>
                                            <div class="body">
                                                <div class="label-discount clear"></div>
                                                <div class="title">
                                                    <a href="{{route('catalog.item', $product->slug)}}">{{$product->product_name}}</a>
                                                </div>
                                                <div class="brand"></div>
                                            </div>
                                            <div class="prices">

                                                <div class="price-current pull-right">{{$product->price}}</div>
                                            </div>
                                            <div class="hover-area">
                                                <div class="wish-compare">
                                                    <a class="btn-add-to-wishlist" href="#">add to wishlist</a>
                                                    <a class="btn-add-to-compare" href="#">compare</a>
                                                </div>
                                            </div>
                                        </div><!-- /.product-item -->
                                    </div><!-- /.product-item-holder -->
                                    @endforeach
                                    @endif
                                </div><!-- /.row -->
                            </div><!-- /.product-grid-holder -->

                            @if(!$products->isEmpty())
                            <div class="pagination-holder">
                                <div class="row">

                                    <div class="col-xs-12 col-sm-6 text-left">
                                        <ul class="pagination ">
                                            <li class="current"><a  href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">next</a></li>
                                        </ul>
                                    </div>

                                    <div class="col-xs-12 col-sm-6">
                                        <div class="result-counter">
                                            Showing <span>1-9</span> of <span>11</span> results
                                        </div>
                                    </div>

                                </div><!-- /.row -->
                            </div><!-- /.pagination-holder -->
                            @endif
                        </div><!-- /.products-grid #grid-view -->

                    </div><!-- /.tab-content -->
                </div><!-- /.grid-list-products -->

            </section><!-- /#gaming -->
        </div><!-- /.col -->
        <!-- ========================================= CONTENT : END ========================================= -->
    </div><!-- /.container -->
</section><!-- /#category-grid -->