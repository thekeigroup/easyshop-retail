<!-- ============================================================= SEARCH AREA ============================================================= -->
<div class="search-area">
    {!! Form::open(array('route'=>'catalog.search', 'method' => 'GET')) !!}
        <div class="control-group">
            <input class="search-field" placeholder="Search for item" name="q" />

            <ul class="categories-filter animate-dropdown">
                <li class="dropdown">

                    <a class="dropdown-toggle"  data-toggle="dropdown" href="category-grid.html">Products</a>
                    <ul class="dropdown-menu" role="menu" >
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Products</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Suppliers</a></li>
                    </ul>
                </li>
            </ul>

            <button type="submit" class="btn search-button"></button>  

        </div>
    {!! Form::close() !!}
</div><!-- /.search-area -->
<!-- ============================================================= SEARCH AREA : END ============================================================= -->