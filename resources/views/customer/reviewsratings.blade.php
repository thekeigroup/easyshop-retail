@extends('layouts._backend')
@section('menu')
    @include('customer.menu')
@endsection
@section('content-title')
    My Reviews And Ratings
@endsection
@section('content')
    @include('forms.customer.review.list')
@endsection