@extends('layouts._backend')
@section('menu')
    @include('customer.menu')
@endsection
@section('content-title')
    Address Book
@endsection
@section('content')
    <a href="{{route('customer.address.create')}}" >Add New Address</a>
    @include('forms.customer.address.list')
@endsection