@extends('layouts._backend')
@section('menu')
    @include('customer.menu')
@endsection
@section('content-title')
    My Orders
@endsection
@section('content')
    @include('forms.customer.order.list')
@endsection