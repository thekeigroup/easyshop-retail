<ul>
    <li><a class="active" href="{{route('customer.dashboard')}}">My Account</a></li>
    <li><a class="" href="{{route('customer.address.index')}}">Addresses</a></li>
    <li><a href="{{route('customer.order.index')}}">My Orders</a></li>
    <li><a href="{{route('customer.reviewsratings.index')}}">Reviews And Ratings</a></li>
    <li><a href="{{route('customer.wishlist.index')}}">My Wishlist</a></li>
    <li class="pull-right"><a href="{{route('logout')}}">Logout</a> </li>
</ul> 
