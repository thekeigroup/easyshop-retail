@extends('layouts._backend')
@section('menu')
    @include('customer.menu')
@endsection
@section('content-title')
    My Wishlist
@endsection
@section('content')
    @include('forms.customer.wishlist.wishlist')
@endsection