<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Supplier extends Model
{
    protected $table = 'supplier';
    protected $fillable = ['first_name', 'last_name', 'email', 'mobile_phone_no', 'preferred_name', 'supplier_type'];

    public function products()
    {
        return $this->hasMany('App\Supplier');
    }

    public function giftCoupons()
    {
        return $this->hasMany('App\GiftCoupon');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }
}
