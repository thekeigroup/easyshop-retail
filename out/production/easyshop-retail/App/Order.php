<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
    //
    protected $table = 'orders';

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function orderstatus()
    {
        return $this->belongsTo('App\OrderStatus');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier', 'supplier_id');
    }

}
