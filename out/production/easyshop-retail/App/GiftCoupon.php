<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class GiftCoupon extends Model
{
    //
    protected $table = 'gift_coupons';

    protected $fillable = ['name', 'code', 'discount', 'start_date', 'end_date', 'status', 'supplier_id'];

    protected $dates = ['start_date', 'end_date', 'created_at', 'updated_at'];

    public function getStartDateAttribute($val, $format = true)
    {
        $value = Carbon::createFromTimestamp(strtotime($val));

        if (true === $format) {
            return $value->format('d-M-Y');
        }

        return $value;
    }

    public function setStartDateAttribute($val)
    {
        $value = Carbon::createFromTimestamp(strtotime($val));
        $this->attributes['start_date'] = $value->toDateString();
    }

    public function getEndDateAttribute($val, $format = true)
    {
        $value = Carbon::createFromTimestamp(strtotime($val));

        if (true === $format) {
            return $value->format('d-M-Y');
        }

        return $value;
    }

    public function setEndDateAttribute($val)
    {
        $value = Carbon::createFromTimestamp(strtotime($val));
        $this->attributes['end_date'] = $value->toDateString();
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier', 'supplier_id');
    }
}