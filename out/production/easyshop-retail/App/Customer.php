<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $table = 'customers';

    protected $fillable = ['first_name', 'last_name', 'gender', 'email_address', 'dob'];

    public function reviews()
    {
        return $this->hasMany('App\Review');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function wishlists()
    {
        return $this->hasMany('App\Wishlist');
    }
}
