<?php

namespace App\Http\Controllers\Supplier;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use App\Supplier;
use App\Http\Controllers\Controller;

class SupplierController extends Controller
{

    //
    public function __construct()
    {
    }

    public function myAccount()
    {
        return view('supplier.my-account');
    }

    public function editSupplierInfo($id)
    {

    }

    public function updateSupplierInfo($id)
    {

    }

    public function getProductImage($filename)
    {
        $file = Storage::disk('local')->get($filename);
        $mimeType = File::mimeType($file);
        $response = Response::make($file, 200);

        $response->header("Content-Type", $mimeType);

        return $response;
    }

    public function getSupplierOrders()
    {

    }

    public function updateOrderStatus(Request $request, $id)
    {

    }

}
