<?php

namespace App\Http\Controllers\Supplier;

use App\OrderStatus;
use App\Order;
use App\Http\Requests\UpdateOrderStatusRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\CommonServiceInterface as CommonService;

class OrderController extends Controller
{

    //
    public function __construct(CommonService $commonService)
    {
        $this->commonService = $commonService;
    }


    public function index()
    {
        $user = Auth::user();
        $supplier = $this->commonService->findSupplierByUserEmail($user->email);

        $orders = $supplier->orders;
        return view('forms.supplier.order.index')
            ->with('orders', $orders);
    }

    public function view($id)
    {
        $order = Order::findorfail($id);
        return view('forms.supplier.order.view')
            ->with('order', $order);
    }

    public function updateOrderStatus($id, UpdateOrderStatusRequest $request)
    {
        $order = Order::findorfail($id);
        $order->update($request->all());

        //TODO
        //email notification to customer

        return redirect()->route('supplier.order.index');
    }

    public function changeStatus($id)
    {
        $order = Order::findorfail($id);
        $orderStatus = OrderStatus::all()->pluck('name', 'id');
        return view('forms.supplier.order.view')
            ->with('order', $order)
            ->with('orderStatus', $orderStatus)
            ->with('changeStatus', true);
    }

}
