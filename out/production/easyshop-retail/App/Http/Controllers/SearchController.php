<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\CommonServiceInterface as CommonService;

class SearchController extends Controller
{
    //
    public function __construct(CommonService $commonService)
    {
        $this->commonService = $commonService;
    }


    public function search(Request $request)
    {
        $q = $request->input('q');
        $products = $this->commonService->searchProducts($q);
        return view('catalog')->with('products', $products);
    }
}
