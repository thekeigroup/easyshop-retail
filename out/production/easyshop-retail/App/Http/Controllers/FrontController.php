<?php

namespace App\Http\Controllers;

use App\Http\Requests\BecomeSupplierRequest;
use App\Http\Requests\SupplierRequest;
use App\Supplier;
use App\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Helpers\CommonServiceInterface as CommonService;
use TCG\Voyager\Models\User;
use TCG\Voyager\Models\Role;

class FrontController extends Controller
{

    public function home()
    {
        return view('index');
    }


    public function signup()
    {
        return view('signup');
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|max:255',
            'mobile_phone_no' => 'required|max:50',
            'pwd' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('become-supplier')
                ->withErrors($validator);
        }
        $role_customer = 'user';
        $customer = new Customer();

        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->gender = $request->gender;
        $customer->email_address = $request->email;
        $customer->dob = $request->dob;


        if ($customer->save()) {
            //create new user
            $user = new User();
            $user->email = $request->email;
            $user->name = $request->last_name . ' ' . $request->first_name;
            $user->password = bcrypt($request->pwd);
            $user->setRole($role_customer);

            $user->save();

            //we will redirect to customer dashboard
        }
    }

    public function login()
    {
        return view('login');
    }

    public function authenticate(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            //authentication password

            $user = Auth::user();


            //get role id from authenticated user
            $role_id = $user->role_id;

            $role_name = Role::find($role_id)->name;

            if ($role_name === 'user') {
                return redirect()->route('customer.my-account');
            }
            if ($role_name === 'supplier' || $role_name === 'admin') {
                return redirect()->route('supplier.my-account');
            }

        } else {
            return redirect()->back()->withInput();
        }
    }

    public function becomeSupplier()
    {
        return view('become-a-supplier');
    }

    public function registerSupplier(SupplierRequest $request)
    {
        if (Supplier::create($request->all())){

            //create user for supplier
            $user = new User();
            $user->email = $request->get('email');
            $user->name = $request->get('last_name') . ' ' . $request->get('first_name');
            $user->password = bcrypt($request->get('password'));
            $role_supplier = 'supplier';
            $user->setRole($role_supplier);
            if ($user->save()) {
                //redirect to login page
                return redirect()->route('login');
            } else {
                return redirect()->back()->withInput();
            }
        }

    }

    public function logout()
    {
        Auth::logout();
        //Redirect to login page
        return redirect()->route('login');
    }

    public function placeOrder()
    {

    }

}
