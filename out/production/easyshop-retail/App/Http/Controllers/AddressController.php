<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Address;
use App\Http\Requests\AddressRequest;



class AddressController extends Controller {

    public function __construct() {

    }

    public function index() {
        $user = Auth::user();
        $addresses = Address::where('user_id', '=', $user->id) -> get();

        return view('customer.address')
            ->with('user', $this->user)
            ->with('addresses', $addresses);

        return view('customer.address');
    }

    public function create() {
        $user = Auth::user();

        return view('forms.customer.address.create')
            ->with('user', $user);
    }

    public function store(AddressRequest $request) {
        $user = Auth::user();
        $request->merge(['user_id' => $user->id]);
        Addresss::create($request->all());
        return redirect()->
            route('customer.address.index');
    }

    public function edit($id) {
        $user = Auth::user();
        $address = Address::findOrfail($id);
        return view('forms.customer.address.edit')
            ->with('address', $address)
            ->with('user', $user);
    }

    public function update(AddressRequest $request, $id) {
        $address = Address::findOrfail($id);
        $address->update($request->all());
        return redirect()->route('customer.address.index');
    }

    public function destroy($id) {
        Address::destroy($id);
        return redirect()->route('customer.address.index');
    }

}
