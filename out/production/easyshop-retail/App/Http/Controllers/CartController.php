<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Collection;

class CartController extends Controller
{

    //
    public function addToCart(Request $request)
    {
        $cart = (null === Session::get('cart')) ? Collection::make([]) : Session::get('cart');

        $qty = (null === $request->get('qty')) ? 1 : $request->get('qty');

        $product = Product::where('slug', '=', $request->get('slug'))->first();
        $productAttributes = [];
        if ($cart->has($product->id)) {
            $item = $cart->pull($product->id);
            $item['qty'] += $qty;
            $cart->put($product->id, $item);
        } else {
            $price = $product->price;

            $cart->put($product->id, [
                'id' => $product->id,
                'qty' => $qty,
                'price' => $price,
                'name' => $product->product_name,
            ]);
        }

        Session::put('cart', $cart);

        return redirect()->back()
            ->with('notificationText', 'Product Added to Cart Successfully!');
    }

    public function view()
    {
        $cartProducts = Session::get('cart');
        return view('cart.view') - with('cartProducts', $cartProducts);
    }

    public function update(Request $request)
    {
        $cartData = Session::get('cart');

        if ($request->get('qty') == 0) {
            $cartData->pull($request->get('id'));
        } else {

            $item = $cartData->pull($request->get('id'));
            $item['qty'] = $request->get('qty');
            $cartData->put($request->get('id'), $item);
        }
        Session::put('cart', $cartData);

        return redirect()->back();
    }

    public function destroy($id)
    {
        $cartData = Session::get('cart');
        $cartData->pull($id);

        Session::put('cart', $cartData);

        return redirect()->back();
    }

}
