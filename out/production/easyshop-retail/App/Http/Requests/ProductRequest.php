<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule['product_name'] = "required|max:255";
        $rule['description'] = "required";
        $rule['price'] = "required|max:14";

        return $rule;

        //|regex:/^-?\\d*(\\.\\d+)?$/"
    }
}
