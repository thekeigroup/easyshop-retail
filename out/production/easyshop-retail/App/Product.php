<?php

namespace App;

use App\Http\Requests\ProductRequest;
use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['product_name', 'description', 'slug', 'published', 'sku', 'in_stock', 'track_stock'
        , 'page_title', 'page_description', 'supplier_id'];

    public function supplier()
    {
        return $this->belongsTo('App\Product');
    }

    public function prices()
    {
        return $this->hasMany('App\ProductPrice');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review');
    }

    public function images()
    {
        return $this->hasMany('App\ProductImage');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function saveProduct(ProductRequest $request)
    {
        //saving or updating product prices
        if ($this->prices()->get()->count > 0) {
            $this->prices()->get()->first()->update([
                'price' => $request->get('price')
            ]);
        } else {
            $this->prices()->create(['price' => $request->get('price')]);
        }

        //saving or updating product images
        if (null !== $request->get('image')) {
            $existingIds = $this->images()->get()->pluck('id')->toArray();

            foreach ($request->get('image') as $key => $data) {
                if (is_int($key)) {
                    if (($findKey = array_search($key, $existingIds)) !== false) {
                        $productImage = ProductImage::findorfail($key);
                        $productImage->update($data);

                        unset($existingIds[$findKey]);
                    }
                    continue;
                }

                ProductImage::create($data + ['product_id' => $this->id]);

            }
            if (count($existingIds) > 0) {
                ProductImage::destroy($existingIds);
            }
        }
    }

    public static function getProductBySlug($slug)
    {
        $model = new static;
        return $model->where('slug', '=', $slug)->first();
    }

    public function getPriceAttribute()
    {
        $row = $this->prices()->first();
        return (isset($row->price)) ? $row->price : null;
    }

    public function getImageAttribute()
    {
        $row = $this->images()->where('is_main_image', '=', 1)->first();
        return $row->path;
    }

}
