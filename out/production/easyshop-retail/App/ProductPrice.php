<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    //
    protected $table = 'product_prices';
    protected $fillable = ['price', 'product_id'];

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
}
