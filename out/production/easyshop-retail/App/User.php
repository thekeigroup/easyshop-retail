<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function getFullNameAttribute() {
        
    }
    
    public function addresses() {
        return $this->hasMany(Address::class);
    }
    
    public function getShippingAddress() {
        $address = $this->addresses()->where('type', '=', 'SHIPPING')->first();
        return $address;
    }
    
    public function getBillingAddresss() {
        $address = $this->addresses()->where('type', '=', 'BILLING')->first();
        return $address;
    }
    
    public function isInWishlist($productId) {
        $wishList = WishList::where('user_id', '=', $this->attributes['id'])
                ->where('product_id', '=', $productId)->get();
        
        if(count($wishList) <= 0) {
            return false;
        }
        
        return true;
    }
}
