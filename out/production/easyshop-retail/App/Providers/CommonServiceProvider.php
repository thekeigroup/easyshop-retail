<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\CommonService;

class CommonServiceProvider extends ServiceProvider
{

    protected $defer = true;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('App\Helpers\CommonServiceInterface', function($app){
            return new CommonService();
        });
    }

    public function provides(){
        return ['App\Helpers\CommonServiceInterface'];
    }
}
